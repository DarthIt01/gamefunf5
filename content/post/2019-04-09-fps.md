---
title: First Person Shooter Game
subtitle: FPS
date: 2019-04-09
tags: ["example"]
---

{{<figure src="/img/fps.png" >}}

<p>FPS Prototype made with Godot Engine!</p>
<p>Click the links and have fun:</p>

1.[ FPS 2.0 version](https://darthit01.gitlab.io/luine-games-godot-fps2.0/)

2.[ FPS 3.0 (Better Graphics, but lack of :( Performance)](https://darthit01.gitlab.io/luine-games-godot-fps3.0/)

<p>Controls are the same of a 1st Person Shooter Game. You can Sprint (shift), throw Grenades (keyboard key: g), change grenades (key: v), grab objects (left mouse button), and much more!</p>

<!--more-->

